---
title: 15 environmental documentaries to watch at home for free!
description: We've listed some interesting, freely available documentaries about climate change, nature and environmetnalism.
tags: [resources, documentaries]
---

While we're all still locked in our homes or student housing, there is no better pastime than watching some good ol' Netflix.
Admittedly, Netflix contains some exquisite documentaries about nature, ecology and climate activism, but some of us can't keep leeching off our ex-girlfriend's cousin's subscription forever.

To solve this pressing need, we've done some serious investigative internet research and have identified some sources where you can watch top documentaries about topics that are close to our hearts, *for free* (and legally).
For instance, the website [documentarymania.com](https://www.documentarymania.com) offers some blockbuster titles in the categories [climate change](https://www.documentarymania.com/results.php?search=Climate&genre=) and [environmentalism](https://www.documentarymania.com/results.php?search=Environmentalism&genre=).
If you're more into light-hearted talks, we've also located some [TED-talks](https://www.youtube.com/playlist?list=PL6VWnVL2NlnS0vNOktUznLmq6Q2bxpAYy) about interesting topics.

For those of you who can't wait to start researching climate change, or just crave some interesting screen time, below are our 15 picks for freely available documentaries. (We haven't watched them all, but hey, we also need to study!)

## ⭐ David Attenborough: A Life on Our Planet (2020)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=David+Attenborough%3A+A+Life+on+Our+Planet)

One man has seen more of the natural world than any other. This unique feature documentary is his witness statement. 


## Artifishal (2019)
[▶️ Watch here!](https://www.youtube.com/watch?v=XdNJ0JAwT7I)

Artifishal is a film about people, rivers, and the fight for the future of wild fish and the environment that supports them. It explores wild salmon’s slide toward extinction, threats posed by fish hatcheries and fish farms, and our continued loss of faith in nature.

## Ice on Fire (2019)
[▶️ Watch here!](https://www.youtube.com/watch?v=WgAflG6MW1c)

Can we reverse climate change? Ice on Fire explores the many ways we reduce carbon inputs to the atmosphere and, more important, how to "draw" carbon down, bringing CO2 out of the atmosphere and thus paving the way for global temperatures to go down.

## ⭐ Before the flood (2016)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=Before%20the%20Flood)

A look at how climate change affects our environment and what society can do to prevent the demise of endangered species, ecosystems and native communities across the planet. 

## Anthropocene: The Human Epoch (2018)
[▶️ Watch here!](https://ihavenotv.com/anthropocene-the-human-epoch)

Filmmakers travel to six continents and 20 countries to document the impact humans have made on the planet. 

## The Undamaged (2018)
[▶️ Watch here!](https://balkanriverdefence.org/the-undamaged/)

Follow a crew of kayakers standing up for the last wild rivers of Europe, threatened by 2700 dams. From Slovenia to Albania they protest and paddle supporting the locals fighting along the way. 

## ⭐ The Ivory Game (2016)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=The%20Ivory%20Game)

Wildlife activists in take on poachers in an effort to end illegal ivory trade in Africa. 

## An Incovenient Sequel (2017)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=An%20Inconvenient%20Sequel%20Truth%20to%20Power)

A decade after An [Inconvenient Truth (2006)](https://www.imdb.com/title/tt0497116) brought climate change to the heart of popular culture, the follow-up shows just how close we are to a real energy revolution. 

## Chasing Coral (2017)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=Chasing%20Coral)

Coral reefs around the world are vanishing at an unprecedented rate. A team of divers, photographers and scientists set out on a thrilling ocean adventure to discover why and to reveal the underwater mystery to the world. 

## This Changes Everything (Naomi Klein Talk)
[▶️ Watch here!](https://www.youtube.com/watch?v=Q8Yyd5dxTGE&list=PLITUAvYMujh9DcjCgyfFIObD8TWZd1p7N&index=18&t=0s)

Klein has been exploring the interface between environmental degradation and capitalism since her first book, No Logo, appeared in 1999.  Her provocative new book, This Changes Everything, argues that carbon is not the ultimate cause of climate change; the real enemy is capitalism.

## How to Change the World (2015)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=How%20to%20Change%20the%20World)

In 1971, a group of friends sail into a nuclear test zone, and their protest captures the world's imagination. Using never before seen archive that brings their extraordinary world to life, How To Change The World is the story of the pioneers who founded Greenpeace and defined the modern green movement.

## Catching the Sun (2016) 
[▶️ Watch here!](https://www.youtube.com/watch?v=SRNhcwCNCJw)

An unemployed American worker, a Tea Party activist, and a Chinese solar entrepreneur race to lead the clean energy future. But who wins and who loses the battle for power in the 21st century? 

## ⭐ Our Planet (2019)
[▶️ Watch here!](https://www.youtube.com/playlist?list=PL7rb3uMaYmjHqT_JUcQYCBa4nEtfDKuSa)

Documentary series focusing on the breadth of the diversity of habitats around the world, from the remote Arctic wilderness and mysterious deep oceans to the vast landscapes of Africa and diverse jungles of South America. 

## ⭐ A Plastic Ocean (2016)
[▶️ Watch here!](https://www.documentarymania.com/player.php?title=A%20Plastic%20Ocean)

Journalist Craig Leeson teams up with diver Tanya Streeter and an international team of scientists and researchers, and they travel to twenty locations around the world over the next four years to explore the fragile state of our oceans. 

## One Strange Rock (2018)
[▶️ Watch here!](https://ihavenotv.com/series/one-strange-rock)

 The extraordinary story of Earth and why it is special and uniquely brimming with life among a largely unknown but harsh cosmic arena; astronauts tell the story of Earth through a unique perspective. 


