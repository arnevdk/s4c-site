---
layout: post
title: 'INFOGRAPHIC: Actionable steps for forested and freshwater ecosystem conservation'
description: "You’ve heard it before: we need to protect Earth’s forests. They provide clean air, store carbon, function as critical habitats, regulate water cycles and the list goes on. At this point it has been etched in our minds that forests are essential. Then why are we as a global community failing to meet conservation targets?"
summary:
tags: [ecosystems]
---

You’ve heard it before: we need to protect Earth’s forests. They provide clean air, store carbon, function as critical habitats, regulate water cycles and the list goes on. At this point it has been etched in our minds that forests are essential. Then why are we as a global community  [failing to meet conservation targets](https://www.nationalgeographic.com/science/2020/09/world-missed-critical-deadline-to-safeguard-biodiversity-un-report/#close)?


![Infographic showing how forests and fresh-water ecosystems are related to global environmental threats.](/images/infographic-ecosystems.png)


Throughout 2020, we witnessed increasingly severe repercussions from human-induced ecosystem degradation. Forested and freshwater ecosystems have an interdependent nexus to regulate natural processes that sustain this world. It is an increasingly fragile bond that is deteriorating at the hand of climate change and human activity.


This relationship between ecosystems is severed largely in part through deforestation. Particularly startling this year was the rate at which global fires would wipe out forested ecosystems, consequently [altering freshwater ecosystems](https://news.bloomberglaw.com/environment-and-energy/wildfires-emerge-as-threat-to-water-quantity-across-parched-west). Each year, we face unprecedented wildfires. Fires are a [natural process](http://www.fao.org/3/ca8642en/CA8642EN.pdf) that provide benefits to some forested ecosystems, but we are experiencing more severe fires that are accelerated through human-induced climate change. These severe wildfires not only threaten plants and animals but can result in lasting damage to roots and soil. In a healthy forested ecosystem, tree roots protect soil structures that retain water and regulate a natural flow regime throughout the watershed. Additionally, the absence of vegetation leaves watersheds more vulnerable to flash floods, dangerous mudslides and extended droughts -- threatening the integrity of freshwater ecosystems and the delivery of clean water downstream. 


This is just one example of the progressively dire situations that stem from governmental inaction. The last year may have seemed all doom and gloom, but we have been presented a rare opportunity to reexamine our relationship with nature:


Internationally, we must not only protect but restore these ecosystems. We can must commit to [nature-based solutions](http://www.fao.org/3/ca6842en/CA6842EN.pdf) that optimize natural processes to ensure healthy ecosystems, thriving species and clean water. Through governmental intervention and guided by science, we can conserve 30% of land and water by 2030 to mitigate impacts from climate change.


Individually, we all need to reevaluate how we interact with our surrounding environment. We need to initiate conversations and vocalize concerns -- with both our loved ones and strangers in local government. We can volunteer with local organizations on the frontlines, sharing the weight that is placed on their backs. We can even simply visit our local forest and listen to the life that unfolds.


2020 has ushered a number of crises across all facets of life, but one question remains: will these crises be followed by momentum for climate action? Will civilians pressure lawmakers to prioritize environmental policies; can lawmakers set aside differences and work towards a common goal; and will industry actors reevaluate their production processes and assume responsibility for their environmental impact? I believe that with purposeful voices and persistent pressure, they will.


*Catherine Dolezal*  
*MSc Sustainable Development*  
*KU Leuven*