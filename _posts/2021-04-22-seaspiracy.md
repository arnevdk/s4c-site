---
title: "What can we learn from Seaspiracy (2021)?"
description: "Have you watched Netflix's new documentary, Seaspiracy?
It uncovers the dirty hidden secrets of the fishing industry, especially its destructive impact on aquatic ecosystems."
tags: [documentaries]
---

Have you watched Netflix's new documentary, **Seaspiracy**?

It uncovers the dirty hidden secrets of the fishing industry, especially its destructive impact on aquatic ecosystems.

Did you know...

🦐 Mangroves are a type of coastal vegetation that play a huge role in maintaining aquatic balance! They absorb even more carbon than trees found more inland, prevent soil erosion, are home to a huge number of aquatic and land species, and keep coastal communities safe from storm surges. The shrimping industry is the biggest cause of mangrove destruction.

🦠 50-80% of all oxygen on Earth is produced by our oceans! Microscopic phytoplankton near the water's surface absorb sunlight and carbon and breathe out oxygen. When floating plastics cover the water's surface, these phytoplankton can't access sunlight, so it's much harder for them to breathe.

🦈 Scary films and dramatic news stories have done sharks so wrong--they're a much bigger part of ocean balance than we give them credit for! Without sharks at the top of the food chain, populations of smaller fish explode, run out of food, and eventually starve to death. This means no more food for other animals...including us humans.

🐢 Way scarier than sharks is the practice of bottom trawling: dragging heavy nets across the sea floor to catch shrimp and tiny fish. All kinds of marine life are caught in these nets, including dolphins, sea turtles, and sharks. By the time these animals are removed from the nets, they are already dead or injured. These nets also completely destroy the coastal coral reefs, the most biodiverse ecosystems on Earth.

Watch Seaspiracy to learn more about how our violent overfishing and pollution habits are suffocating and massacring our blue lung, and to learn more about what you can do to protect it.

