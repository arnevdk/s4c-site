---
layout: post
title: "How to stay connected while social distancing - and save the planet in the meantime."
description: "Zero carbon emission communication alternatives: how to stay connected while social distancing - and save the planet in the meantime."
summary: "Video conferencing over the Internet is a useful tool to stay connected to your fellow students or have some fun with your friends. Video streaming services hosted by large datacenters do however consume a significant amount of (unclean) energy. We want to promote some zero carbon alternatives to the usual platforms."
tags: [technology,corona]

---

*Nederlands hieronder*

*In these troubling Corona times, where social contact itself is a health risk, it is often a challenge to stay in touch with your friends, keep up with extracurricular activities or even follow your classes; thank God for all those messaging and video chat apps!
But we wouldn't be the climate activists we are, if we wouldn't question the impact they have on our environment. Today it's time to take a look at what the sudden increase in activity on all those platforms -- and I hope we can agree there are already way too much of them -- is doing to our atmosphere.*

### Data is electricity, computers get hot

Using the Internet is using other people's computers, and with computers we mean a global network of huge data centers with billions of computing nodes. Naturally, it takes a lot of electric power to keep all those computers running, and a lot more energy to keep them cool and efficient. Of course, these data centers don't just run to forward your text message to a friend, but they are shared by a lot of different users. However, with each web resource you request, you bear a fraction of the emission cost that comes with maintaining the data centers.  In 2012, the total greenhouse gas emission footprint of  Internet usage and the systems supporting the Internet was estimated at **3.2% of global emissions**[1] (for comparison, this is about the same amount as produced by the airline industry). It is safe to state that this number has only increased since then.

Ok, but what about video chat? When it comes to Internet usage, video streaming can be one of the most resource intensive tasks you can give your computer -- as you probably will have noticed when everyone at home is watching a different Netflix series at the same time and the Wi-Fi is too slow to binge your own series. Another 2012 study has shown that a 5 hour video call can produce between 4 and 215kg CO2e[2]. 

Does this mean we have to stop using online communication platforms alltogether? **Of course not!** Video conferencing can cause a significant improvement in your carbon footprint when you would otherwise have to drive to a destination to have your in-person meeting you can now host on-line. In fact, you would have to drive 20km by car to cancel out the emissions of a regular video call. There are even [other ways](https://schepisi.com.au/go-green-how-is-lifesize-video-conferencing-environmentally-friendly/) in which on-line conferencing can be better for the environment than face-to-face meetings. Sadly enough, that's not really an option in this quarantine. 

### Communication tools with a 💚 for the planet

What if you really want to play that on-line party game with your friends, or you want to organize an on-line cantus with your  student organization? And what about all those teamwork deadlines crushing in on you by the end of this exceptional on-line semester? Here are our picks for climate friendly communication:

1.  **[Ungleich.ch](https://ungleich.ch)** is a Swiss based company that provides web and communication services. Their data center in Switzerland is entirely powered by zero carbon hydro-electric power and strong Swiss data and privacy protection laws apply. Ungleich hosts a free-to-use video chat platform **[talk.ungleich.ch](https://talk.ungleich.ch/)**, which is built with the open source video conferencing software **[Jitsi](https://jitsi.org)**.![ungleich](https://ungleich.ch/u/image/ungleich-logo-black.svg)
2.  [**Framatalk**](https://framatalk.org/accueil/en/) is free and is maintained by Framasoft, a small French non-profit that promotes digital freedom through ethical projects. Framasoft software supports study groups and activist groups around topics like climate change, social justice and communities.  Framatalk also uses the Jitsi platform.
    ![Framatalk : parfait pour remplacer Skype - Ouebsson](https://www.ouebsson.fr/wp-content/uploads/sites/3/2017/11/framatalk_logo.png)
3.  **Host your own communication!** If you're satisfied with these solutions, you can stop reading here, but if you're a geek like me: there is a lot more you can do. If you're a bit tech savvy and want to take your zero carbon emission communication to a whole new level, you can set up your own communication infrastructure. All you need is a carbon friendly hosting provider (which admittedly might be hard to come by) or your own server which for instance runs of your houses solar panels. As an example,  **[Ungleich.ch](https://ungleich.ch)** hosts some messaging platforms like **[Matrix](https://matrix.org)** and **[Mattermost](https://mattermost.com/)** which both provide a collaborative chat instance and the video conferencing platform **Jitsi**. Note that this option often comes at a financial cost to support the infrastructure and you will have to spend some time to set it all up. This might be a viable alternative if you are part of an organization that requires an internal communication platform and that is able to support you.

It's hard to track the exact emissions of these platforms (holistic impact analysis is *very* hard), but at least they make an effort to decrease their environmental impact, which we can only encourage. You'll probably still have some environmental impact while using these platforms, and within the bigger picture of our current global situation this might not be a battle worth fighting, but what else can we do but strive for loving our planet in everything we do?



<br>
<br>
<hr>
<br>
<br>


*In deze moeilijke Corona-tijden, wanneer zelfs sociaal contact een gezondheidsrisico is, is het vaak een uitdaging om in contact te blijven met je vrienden, deel te nemen aan buitenschoolse activiteiten of zelfs je lessen te volgen; gelukkig zijn er al die messaging- en videochat apps!
Maar we zouden niet die klimaatactivisten zijn als we niet zouden twijfelen aan het effect dat deze apps hebben op ons milieu, dus vandaag is de tijd om te kijken naar wat de plotselinge toename van de activiteit op verschillende communicatieplatforms platforms -- en ik hoop dat we het erover eens kunnen zijn dat er in elk geval veel te veel verschillende zijn-- doet met onze atmosfeer.*

### Data is elektriciteit, computers worden warm

Het Internet gebruiken is andermans computers gebruiken, en met computers bedoelen we dan een wereldwijd netwerk van enorme datacenters met miljarden servers. Natuurlijk kost het veel stroom om al die computers draaiende te houden, en nog veel meer energie om ze koel en efficiënt te houden. Natuurlijk draaien deze datacenters niet alleen om je chatbericht door te sturen naar een vriend, maar worden ze door veel verschillende gebruikers gedeeld. Toch draag je met elke webpagina die je bezoekt of met elk bericht dat je verstuurt een fractie van de emissiekosten die gepaard gaan met het onderhoud van die datacenters.  In 2012 werd de totale emissievoetafdruk van Internetgebruik en systemen die het Internet ondersteunen geschat op **3,2% van de wereldwijde uitstoot**[1] (ter vergelijking, dit is ongeveer evenveel als de luchtvaartindustrie). Het is veilig om te stellen dat deze uitstoot sindsdien alleen maar is toegenomen.

Oké, maar hoe zit het met de videochat? Als het gaat om Internetgebruik, kan videostreaming een van de meer intensieve taken zijn -- zoals je waarschijnlijk al hebben gemerkt hebt wanneer iedereen thuis op hetzelfde moment een andere serie aan het kijken is en de Wi-Fi is te traag om je eigen serie te bingen. Een ander onderzoek uit 2012 heeft aangetoond dat een 5 uur durend videogesprek tussen 4 kg CO2e en 215 kg CO2e kan produceren[2]. 

Betekent dit dat we helemaal moeten stoppen met het gebruik van online communicatieplatforms? **Natuurlijk niet!** Videoconferencing kan een aanzienlijke verbetering in CO2-voetafdruk veroorzaken wanneer je anders naar een bestemming zou moeten rijden voor een face-to-face meeting. Je zou ongeveer 20 km met de auto moeten rijden om evenveel uit te stoten als een doorsnee videogesprek. Er zijn zelfs [andere manieren](https://schepisi.com.au/go-green-how-is-lifesize-video-conferencing-environmentally-friendly/) waarop online vergaderen beter kan zijn voor het milieu dan face-to-face vergaderingen. Helaas is dat niet echt een optie in deze quarantaine. 

### Communicatie met een 💚 voor de planeet

Wat als je dan echt die online partygame wilt spelen met je vrienden, of je wilt een online cantus organiseren met je studentenorganisatie? En hoe zit het met al die teamworkdeadlines die tegen het einde van dit online semester af moeten zijn? Hier zijn onze keuzes voor klimaatvriendelijke communicatie:

1.  **[Ungleich.ch](https://ungleich.ch)** is een Zwitsers bedrijf dat web- en communicatiediensten aanbiedt. Hun datacentrum in Zwitserland wordt volledig gevoed door hydro-elektrische energie zonder uitstoot van broeikasgassen. Er gelden sterke Zwitserse wetten voor privacy en gegevensbescherming. Ungleich biedt onder andere een gratis videochatplatform **[talk.ungleich.ch](https://talk.ungleich.ch/)** aan, dat is gebouwd met de open source software voor videoconferenties **[Jitsi](https://jitsi.org)**.![ungleich](https://ungleich.ch/u/image/ungleich-logo-black.svg)
2.  [**Framatalk**](https://framatalk.org/accueil/en/) is gratis en wordt onderhouden door Framasoft, een Franse non-profitorganisatie die digitale vrijheid promoot door middel van ethische projecten. Software van Framasoft ondersteunt studie- en activismegroepen rond onderwerpen zoals klimaatverandering, sociale rechtvaardigheid en gemeenschappen.  Framatalk maakt ook gebruik van het Jitsi-platform.
    ![Framatalk : parfait pour remplacer Skype - Ouebsson](https://www.ouebsson.fr/wp-content/uploads/sites/3/2017/11/framatalk_logo.png)
3.  **Host je eigen communicatie!** Als je tevreden bent met de oplossingen hierboven, kan je hier stoppen met lezen, maar als je een geek bent zoals ik: er is nog veel meer mogelijk. Als je een beetje van technologie kent en je wilt communiceren door zo min mogelijk CO2 uit te stoten, kun je je eigen communicatie-infrastructuur opzetten. Het enige wat je nodig hebt is een CO2-vriendelijke hostingprovider (dit weliswaar niet zo gemakkelijk te vinden), of je kan je eigen server runnen als je bijvoorbeeld zonnepanelen hebt thuis. **[Ungleich.ch](https://ungleich.ch)** host bijvoorbeeld  tegen betaling een aantal messaging platforms zoals **[Matrix](https://matrix.org)** en **[Mattermost](https://mattermost.com/)**. Deze optie gaat wel vaak gepaard met financiële kosten om de infrastructuur te ondersteunen en je zult wat tijd moeten besteden aan het opzetten ervan. Dit kan een mogelijkheid zijn als je deel uitmaakt van een organisatie die een intern communicatieplatform nodig heeft en die in staat is om je hierin te indersteunen.

### References

1. [Ferreboeuf, Hugues, et al. “‘Lean ICT: Towards Digital Sobriety’: Our New Report.” *The Shift Project*, 8 Jan. 2020,   theshiftproject.org/en/article/lean-ict-our-new-report/.](https://theshiftproject.org/wp-content/uploads/2019/03/Lean-ICT-Report_The-Shift-Project_2019.pdf)
2. [Ong, Dennis, Tim Moors, and Vijay Sivaraman. "Comparison of the energy,  carbon and time costs of videoconferencing and in-person meetings." *Computer communications* 50 (2014): 86-94.](http://www2.eet.unsw.edu.au/~vijay/pubs/jrnl/14comcomVC.pdf)


