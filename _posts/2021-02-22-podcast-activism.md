---
layout: post
title: "PODCAST: 
Getting Started With Activism and Keeping it Sustainable w\ Talking with Apes"
description: "Talking with Apes wants to fix planet Earth.
They kindly invited us to feature in a podcast about climate activism."
summary: "Talking with Apes wants to fix planet Earth.
They kindly invited us to feature in a podcast about climate activism."
tags: [podcast,activism]
---

Talking with Apes wants to fix planet Earth.
They kindly invited us to feature in a podcast about climate activism.
Check out their [Facebook](https://www.facebook.com/TalkingWithApes) page!

<iframe width="560" height="315" src="https://www.youtube.com/embed/vTwCaFOgu0Y" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
