---
layout: post
title: 'How to prevent a new pandemic: a new perspective on agro-industry'
description: Our current system of food production can cause pandemic outbreaks.
summary: Our current system of food production can cause pandemic outbreaks. Monocultures increase the risk of cross-species infections.
tags: [corona]
---

![mono-culture](/images/agro-industry.png)


*Nederlands hieronder*

> "The biggest problem with new epidemics is not a missing vaccine, but the unwillingness to understand that those epidemics are not accidental."

Rob Wallace, author of Big Farms make Big Flu, [mentioned it earlier in his book](https://lavamedia.be/covid-19-landroof-en-agro-business-aan-de-basis-van-epidemieen/).
There is a structural problem in our dealings with nature.
The agro-industry buys up land from smaller players and cultivates new land in rainforests.
In those old woods and rainforests, there was much more biodiversity, so pathogens remained trapped.
Today, the agro-industry is engaged in **gigantic monoculture and livestock farming**.
Monoculture is exactly the opposite of biodiversity.
A monoculture grows a single crop over gigantic areas.
**Without this crucial biodiversity, diseases spread rapidly**.
Unfortunately, this is also the most profitable way to produce food: the cheap land in the Amazon forest, the intensive use of pesticides and packing anmials together as efficiently as possible.
We then speak of a cost effective and not an efficiency for society.

How can we prevent another Corona virus?
And how can this help us in the fight against global warming?
Actually, these two go hand in hand.
We need to break with the profit-driven, polluting and destructive agro-industry.
This requires an agro-ecological model that takes planet and human into account.
Vary food production, bring different crops closer togethe.
Leave land barren, so that the land does not become exhausted.
Provide food production that meets people's needs.
In other words: no gigantic production of meat or other overproduction.
This also requires production in the hands of people.
Today you can already see some good initiatives, such as allotments.
Gardens with varied crops, planted and harvested by ordinary people.
That is the right principle, but it is not enough.
The entire agro-industry needs to be transformed into a social model of agriculture that takes people and the planet into account.
In this way, we can combat climate change and create a barrier against new viruses.
<br/><br>
<hr/>
<br/>

>  "Het grootste probleem bij nieuwe epidemieën is niet een ontbrekend vaccin, maar de onwil om te begrijpen dat die epidemieën niet toevallig zijn."

Rob Wallace, auteur van Big Farms make Big Flu, [benoemde het eerder al in zijn boek](https://lavamedia.be/covid-19-landroof-en-agro-business-aan-de-basis-van-epidemieen/), voordat er sprake was van het Corona-virus.
Er zit een structureel probleem in onze omgang met de natuur.
De agro-industrie koopt land op van kleinere spelers en ontgint nieuwe gronden in de regenwouden.
In die oude regenwouden en bossen was er veel meer biodiversiteit, waardoor ziekteverwekkers ingesloten bleven. Vandaag doet die agro-industrie aan **gigantische monocultuur en veeteelt**.
Monocultuur is precies het omgekeerde van biodiversiteit.
Een monocultuur verbouwt één gewas over gigantische oppervlaktes.
**Zonder die biodiversiteit verspreiden ziektes razendsnel**.
Dat is jammer genoeg ook de meest winstgevende manier om voedsel te produceren:
de goedkope gronden in het Amazone woud, het intensief gebruik van pesticiden en het efficiënt opeenhopen van dieren.
We spreken dan van een kostenefficiënt en niet efficiëntie voor de maatschappij.

Hoe kunnen we een volgend Corona-virus voorkomen?
En hoe kan dit ons vooruit helpen met de strijd tegen de klimaatopwarming?
De twee gaan eigenlijk hand in hand. We moeten breken met de winst gedreven, vervuilende en vernielende agro-industrie.
Dat vereist een agro-ecologisch model dat rekening houdt met planeet en mens. Varieer in de voedselproductie, zet verschillende gewassen dichter bij elkaar.
Laat gronden braak liggen, zodat de gronden niet uitgeput raken.
Voorzie een voedselproductie die voldoet aan de noden van de mensen.
Dat wil zeggen: geen gigantische productie van vlees of andere overproductie.
Daarnaast vereist dat ook een productie in de handen van mensen.
Vandaag zie je al enkele goede initiatieven, zoals volkstuintjes.
Tuintjes met gevarieerde gewassen, geplant en geoogst door gewone mensen.
Dat is het juiste principe, alleen volstaat dit niet.
De hele agro-industrie moet getransformeerd worden in een sociaal landbouwmodel dat rekening houdt met mens en planeet.
Op die manier kunnen we de klimaatverandering tegengaan, en een dam vormen tegen nieuwe virussen


