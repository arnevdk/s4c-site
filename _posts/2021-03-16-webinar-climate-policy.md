---
layout: post
title: "WEBINAR: Belgium's Climate Policy for International Students"
description: "Does Belgium have a decent climate policy? How does Belgium's multitude of governments work together to reduce carbon emissions? Is Belgium on track to meet the Paris Agreement's emission reductions? These questions and more will be answered in this webinar organized in cooperation with KU Leuven's international student platform Pangaea. Get to know Belgium by investigating how it deals with one of the most fundamental problems of our time."
summary:
tags: [government, webinar]
---

Does Belgium have a decent climate policy? How does Belgium's multitude of governments work together to reduce carbon emisisons? Is Belgium on track to meet the Paris Agreement's emission reductions? These questions and more will be answered in this webinar with knowledgeable experts, organised in cooperation with KU Leuven's international student platform Pangaea. Get to know Belgium by investigating how it deals with one of the most fundamental problems of our time.



We first welcome Laurien Spruyt. Laurien is an engineer and an expert in climate policy an sustainable development. She works at _Bond Beter Leefmilieu_, and analyses the climate policy of Flanders, Belgium and Europe. Her research and lobby work mostly focuses on mobility and transport.


Our second speaker is professor Kris Bacchus. Since 2002, he is research leader environmental policy at KU Leuven's _HIVA_ Institute. Kris Bacchus has a multidisciplinary background in economics, social and labor sciences, environmental science.


[▶️ Rewatch the webinar here!](https://eu-lti.bbcollab.com/recording/3cb75a8948ce4d8f990b53c9a061a675)