---
description: Muurschildering
summary: Muurschildering
tags:
- corona
- action
title: Muurschildering
---

# Onderwerp

Deze muurschildering geeft een enorme radiator weer. Het
verwarmingstoestel is een metafoor voor de razendsnelle en
problematische opwarming van het klimaat. De radiator hangt aan de
buitenkant van deze muur, omdat we net die buitenlucht aan het opwarmen
zijn. Het staat symbool voor de schaal van het probleem en de rol die
mensen er in spelen.

De muurschildering is een call for action om die opwarming af te remmen
en vast te blijven houden aan de grens van 1,5° C die tijdens het
akkoord van Parijs vooropgesteld werd. We hopen met deze muurschildering
een krachtig statement in het Leuvense straatbeeld achter te laten en op
die manier meer sensibilisering omtrent de klimaatcrisis in de hand te
werken.

# Proces

De klimaatcrisis wint de laatste jaren gelukkig aan belangstelling, maar
in maart 2020 viel het klimaatdebat plots stil. De eerste golf van
COVID-19 legde onze samenleving plat en eiste abrupt alle media-aandacht
op. Plots leek de klimaatcrisis niet meer het grootste probleem dat onze
samenleving teisterde, al was niets minder waar.

Tijdens die eerste maanden van de coronacrisis begon bij leden van Youth
for Climate Leuven en Students for Climate Leuven het idee op te
borrelen om de klimaatcrisis weer 'letterlijk' in the picture te
plaatsen. Het plan ontstond om de vele wandelende lockdown-lijders een
geweten te schoppen door hen aan de hand van een gigantische
muurschildering te confronteren met de ernst van de klimaatcrisis.

Jongeren en studenten uit Leuven sloegen de handen ineen om dit verhaal
mogelijk te maken. We vonden met Bisser de ideale artiest om dit project
uit te werken en konden bij [MijnLeuven](https://www.mijnleuven.be/) en
[30CC](https://www.30cc.be/nl) aankloppen voor de nodige financiële
ondersteuning.

Nu de coronacrisis op haar laatste benen loopt, is die muurschildering
na twee jaar eindelijk een feit. Op zaterdag 23 april legt Bisser de
laatste hand aan de muurschildering en organiseren we een plechtige
opening. Het evenement zal deel uitmaken van de [Warm
Alarm-week](https://www.30cc.be/nodes/warmalarm) van 30CC.
