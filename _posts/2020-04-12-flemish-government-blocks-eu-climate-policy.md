---
layout: post
title: 'The Flemish Government must show more (and not less) climate ambition'
description: 
summary:
tags: [corona,eu,'green deal',government]

---



**Do you want to fix this issue? Support the [petition](https://openletter.earth/open-brief-aan-minister-demir-4369dd1c) of Extinction Rebellion Youth**

*Nederlands hieronder*

*What after corona? After this crisis, it is important to rebuild normal life, but in a way that has sustainability and social justice at heart. Unfortunately, this is not what our Flemish government wants. This week it became clear that they choose to maintain the current toxic system, instead of building a positive future, a future in which we avoid crises.*

**This week, Zuhal Demir, Flemish Minister for the Environment and Energy, put the breaks on a widely supported proposal** from Denmark to take the European Green Deal as the basis of the recovery policy after the corona crisis. This proposal warns against the temptation of short-term solutions that will make the European Union dependent on fossil fuels for a long time to come.

The Flemish Government does not oppose this proposal because it believes that the Green Deal is not ambitious enough and that Belgium should do better, but because within the Green Deal policy and within the support policy around the corona crisis, Wallonia would receive more money than Flanders, according to [De Standaard](https://www.standaard.be/cnt/dmf20200409_04918780), [De Tijd](https://www.tijd.be/dossier/europareeks/vlaanderen-trekt-neus-op-voor-green-deal/10219922.html) and [EurActiv](https://www.euractiv.com/section/politics/news/flanders-chooses-eu-green-deal-as-battleground-between-regions/). The government believes that we should be 'eco-realistic' and cannot consent until there is clarity about the exact cost of the coronacrisis. Under the guise of 'ecorealism', the Flemish government has already actively counteracted climate measures on several occasions. They choose to [not even do the minimum](https://www.knack.be/nieuws/belgie/als-er-op-grotere-schaal-moet-worden-nagedacht-dan-gaan-bij-n-va-de-hakken-in-de-droge-vlaamse-grond/article-opinion-1587379.html). This policy is not at all about smart use of money. Rather, it ensures that the Flemish Government does not have to roll up its sleeves and that it can get on with 'business as usual'. In this scenario, the [large polluting companies](https://www.demorgen.be/nieuws/hypocriet-belgie-geeft-meer-subsidies-aan-stookolie-dan-aan-renovatie~bd1770f9/) will get advantages over people and nature. We cannot tolerate this.

The EU sees the Green Deal as an ambitious plan with which it wants to set the tone for the rest of the world. In reality, this Deal is the absolute minimum that Europe can do to tackle the climate crisis, which implies that the Flemish Government would like to do less than the minimum. So says [Greenpeace](https://www.greenpeace.org/nl/natuur/29824/green-deal-fundamentele-omslag-nodig/): 

> "The Green Deal contains important reforms and good intentions, but it does not achieve what science tells us is required to deal with the climate and nature crisis."

The Green Deal aims to reconcile the myth of infinite economic growth with a sustainable policy, but even the European Environment Agency says the two are incompatible.

Because our government is now stepping on the brakes, **it is holding the entire country's climate policy hostage**. This would not be possible in a country where there is only one climate minister. One minister, who effectively looks at the global situation and doesn't just bury his head in his own sand. After the corona crisis, we need an even more ambitious Green Deal, a well worked out plan that sets the tone for the new society. This should serve as a basis for the recovery measures after the corona crisis and the support policy should be elaborated with a focus on sustainability. Let's fight together for a sustainable and social Belgium with 1 climate minister instead of 4 different ones that only sow more division. 
<br/><br/>
<hr/>
<br/>
*Wat na corona? Na deze crisis is het belangrijk om het normale leven terug op te bouwen, maar wél op een manier die duurzaamheid en sociale rechtvaardigheid in het hart draagt. Dat is helaas niet wat onze Vlaamse regering wil. Deze week werd het duidelijk dat zij kiezen om het huidige toxische systeem in stand te houden, in de plaats van een positieve toekomst uit te bouwen, een toekomst waarin we crisissen voorkomen.*

**Zuhal Demir, Vlaams Minister van Omgeving en Energie, gaf deze week tegengas aan een breed gedragen voorstel** uit Denemarken om de Europese Green Deal als basis te nemen van het recuperatiebeleid na de coronacrisis. Dit voorstel waarschuwt tegen de verleiding van oplossingen op korte termijn die ervoor zorgen dat de Europese Unie nog lang afhankelijk van fossiele brandstoffen zal zijn.

Dit tegenwerken doet ze niet omdat ze vindt dat de Green Deal niet genoeg is en België het beter moet doen, maar wel omdat binnen het Green Deal-beleid en binnen het steunbeleid rond de coronacrisis Wallonië meer geld zou krijgen dan Vlaanderen, zo melden [De Standaard](https://www.standaard.be/cnt/dmf20200409_04918780), [De Tijd](https://www.tijd.be/dossier/europareeks/vlaanderen-trekt-neus-op-voor-green-deal/10219922.html) en [EurActiv](https://www.euractiv.com/section/politics/news/flanders-chooses-eu-green-deal-as-battleground-between-regions/). Zij vindt dat we ‘ecorealistisch’ moeten zijn en niet kunnen toestemmen tot er duidelijkheid is over de exacte kostprijs van de coronacrisis. Onder het mom van ‘ecorealisme’ werkte de Vlaamse overheid al vaker actief klimaatmaatregelen tegen. Ze kiezen ervoor om [niet eens het minimum te doen](https://www.knack.be/nieuws/belgie/als-er-op-grotere-schaal-moet-worden-nagedacht-dan-gaan-bij-n-va-de-hakken-in-de-droge-vlaamse-grond/article-opinion-1587379.html). Dit beleid gaat helemaal niet over slim omgaan met geld. Het zorgt er eerder voor dat de Vlaamse Overheid de handen niet uit de mouwen moet steken en dat ze verder kan met de ‘business as usual’. In dit scenario krijgen de [grote vervuilende bedrijven](https://www.demorgen.be/nieuws/hypocriet-belgie-geeft-meer-subsidies-aan-stookolie-dan-aan-renovatie~bd1770f9/) voordelen op kap van mens en natuur. Zo kunnen we dus niet verder. 

De EU ziet de Green Deal als een ambitieus plan waarmee ze de toon voor de rest van de wereld wil zetten. In realiteit is deze Deal het absolute minimum wat Europa kan doen om de klimaatcrisis aan te pakken, wat impliceert dat de Vlaamse Overheid minder dan het minimum zou willen doen. Zo zegt [Greenpeace](https://www.greenpeace.org/nl/natuur/29824/green-deal-fundamentele-omslag-nodig/): 

> "In de Green Deal staan belangrijke hervormingen en goede voornemens, maar hiermee bereiken we niet wat volgens de wetenschap vereist is om de klimaat- en natuurcrisis het hoofd te bieden."

De Green Deal wil het fabeltje van oneindige economische groei verzoenen met een duurzaam beleid, maar zelfs het Europees Milieu Agentschap zegt dat die twee niet verenigbaar zijn.

Doordat onze regering nu volop op de rem gaat staan, **houdt ze het klimaatbeleid van hele land gegijzeld**. Dit zou niet mogelijk zijn in een land waar er slechts één klimaatminister is. Eén minister, die effectief naar de globale situatie kijkt en niet enkel de kop in eigen zand steekt. Na de coronacrisis hebben we nood aan een nog ambitieuzere Green Deal, een goed uitgewerkt plan, dat de toon zet voor de nieuwe samenleving. Deze moet als basis dienen van de herstelmaatregelen na de coronacrisis en het steunbeleid moet uitgewerkt worden met een focus op duurzaamheid. Laten we samen vechten voor een duurzaam en sociaal België met 1 klimaatminister in plaats van 4 verschillende die enkel meer verdeling zaaien. 


