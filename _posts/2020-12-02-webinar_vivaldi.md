---
layout: post
title: 'WEBINAR: Doet de Vivaldi-regering genoeg voor het klimaat?'
description: "De klimaatcrisis, hoog tijd dat onze beleidsmakers daar ambitieus mee aan de slag gaan, zeker in tijden van corona!
We hebben nu wel een nieuwe federale regering, maar heeft die wel de nodige klimaatambitie?
Heeft de regering het signaal van de jeugd in 2019 gehoord en dit in hun beleid opgenomen?"
summary:
tags: [government, webinar]
---

![webinar banner](/images/webinar-vivaldi.jpg)

De klimaatcrisis, hoog tijd dat onze beleidsmakers daar ambitieus mee aan de slag gaan, zeker in tijden van corona!
We hebben nu wel een nieuwe federale regering, maar heeft die wel de nodige klimaatambitie?
Heeft de regering het signaal van de jeugd in 2019 gehoord en dit in hun beleid opgenomen?
Sinds 1 oktober 2020 is de regering De Croo I aan de macht. Deze regering bestaat uit MR, Open Vld, SP.a, PS, Groen, Ecolo en CD&V. Met de Green Deal in het vizier vragen wij ons af of deze partijen voldoende tegemoet gaan komen aan de eisen van Europa.
Dé hamvraag is dus: doet de Vivaldi-regering wel genoeg voor het klimaat?
Tijdens dit webinar zullen twee experten hier een uitgebreid antwoord op geven.



Onze eerste spreker is Laurien Spruyt. Zij is ingenieur en experte in klimaatbeleid en duurzame ontwikkeling. Ze werkt bij Bond Beter Leefmilieu en analyseert voor hen het klimaatbeleid van Vlaanderen, België en Europa. Ze is dus dé persoon bij uitstek om hier een genuanceerde en realistische blik op te geven.



Onze tweede spreker is professor Kris Bachus. Hij is sinds 2002 onderzoeksleider in milieubeleid aan HIVA van de KU Leuven. Met zijn multidisciplinaire achtergrond in economie, sociale wetenschappen,
arbeidswetenschappen en milieukunde is hij de geknipte persoon om het regeerakkoord op de rooster te leggen.

[▶️ Bekijk het webinar](/media/Webinar Klimaatbeleid Vivaldi-regering.mp4)