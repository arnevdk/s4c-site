---
title: '#WatNaCorona'
description: Hoe ziet de wereld eruit na de Corona-crisis?
summary: We willen het het hebben over het opbouwen van een duurzame en sociale wereld.
tags: [corona]
---

*Nederlands hieronder*

These unseen times come with a lot of uncertainty, sadness, anger and loss.
We empathize with everyone who has lost someone, is ill, can no longer pay for their studies, has to work in unsafe circumstances, and so many others.
*By no means* this crisis is good for people and climate, even though we see an improvement in air quality and biodiversity is returning in certain places.
**These changes are temporary** and would be reversed when we return to business as usual.
Yet, we also see that, all of a sudden, the problems in our society and our system are being exposed; just look at the residential care centres and the healthcare industry that, that have been subject to major funding cuts, and are now in the front line.
As with climate change, we were not prepared for this.

Hence, it gradually becomes clearer that going back to business as usual is not an option, because this business is built on a system that puts the economy over people and nature.
In times like these climate measures are being phased out to leave large companies out of the wind.
We are in this crisis together and we have to come out of it together, we cannot go back to the way it was and we have to build our society and economy in a sustainable and socially just way.
In the following weeks, we are going to talk about a sustainable and social world after this crisis and we are going to look at what we do and don't want to do in the future. 
<br/><br/>
<hr/>
<br/>
Deze ongeziene tijden brengen veel onzekerheid, verdriet, boosheid en  verlies met zich mee.
We leven mee met iedereen die iemand verloren  heeft, ziek is, zijn of haar studies niet meer kan betalen, moet gaan werken in  onveilige situaties en nog zoveel anderen.
*By no means* is deze crisis  goed voor mens en klimaat, ook al zien we de luchtkwaliteit verbeteren  en biodiversiteit even terugkomen op bepaalde plaatsen.
**Deze veranderingen zijn tijdelijk** en zouden terug opgeheven worden wanneer we terugkeren naar ‘business as usual’. Maar, wat we ook zien is dat  plots de problemen in onze samenleving en ons systeem open en bloot  komen te liggen, kijk maar naar de woonzorgcentra en de zorg, waarop  jarenlang bespaard is en die nu in de frontlinie staat.
Net zoals op de  klimaatverandering waren we hier niet op voorbereid.

Het wordt dus  stilaan duidelijker en duidelijker dat het geen optie is om terug te  gaan naar die zogenaamde ‘business as usual’, omdat die gebouwd is op  een systeem dat economie boven mens en natuur zet en in tijden als deze  klimaatmaatregelen afbouwt om grote bedrijven uit de wind te laten. We zitten samen in deze crisis en moeten er samen ook terug uit komen, we  kunnen niet terug naar hoe het was en moeten onze samenleving en  economie op een duurzame en sociaal rechtvaardige manier opbouwen. We gaan het tijdens deze periode hebben over een duurzame en sociale wereld na deze crisis en we kijken naar wat we wel en niet meer willen naar de toekomst toe. 

![foto van Students For Climate Leuven.](/images/watnacorona.jpg)
