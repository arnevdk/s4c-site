---
layout: page
title: About
---

We are a student organisation in Leuven that strives for an ambitious, effective and socially just climate policy in Belgium and Europe.
We focus on mobilisation and sensibilisation around topics like durability, social justice, the impact of companies on climate policies and many more, as well as current topics in science and politics. 
In addition to our activist calling, we organise fun activities for students in Leuven.


Students for Climate Leuven is a non partisan organisation and an autonomous member of Students for Climate Belgium.
You can read our joint charter, which states our common values and policies, on [studentsforclimate.be](https://studentsforclimate.be)


Find us on Facebook at [facebook.com/studentsforclimateleuven](https://www.facebook.com/studentsforclimateleuven/) and
Instagram at [s4cleuven](https://www.instagram.com/s4cleuven/), or contact us via e-mail at [studentsforclimate.leuven@gmail.com](mailto:studentsforclimate.leuven@gmail.com).
