---
layout: page
title: Join us
---

We are looking for volunteers, activists, and people with a heart for
the world and the environment. Are you a Leuven-based student passionate
to make a positive change? Please do not hesitate to contact us on
[Facebook](https://www.facebook.com/studentsforclimateleuven/) or via
e-mail
[studentsforclimate.leuven@gmail.com](mailto:studentsforclimate.leuven@gmail.com).
