FROM jekyll/builder AS build
WORKDIR /srv/jekyll
COPY Gemfile Gemfile*.lock ./
RUN bundle install
COPY . .
ENV JEKYLL_ENV=production
RUN bundle exec jekyll build  && cp -r ./_site /build 


FROM nginx
COPY --from=build /build /usr/share/nginx/html
